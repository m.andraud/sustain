# Sustain

This repository is the working repository of the SustAIn EU project. It contains links to all blocks developped during the project. 


## Toolflow used in the project development

### Open-source A-core RISC-V processor design 
- A-core is an open-source RISC-V processor environment designed in Aalto University. 
- It can serve as a baseline processor for developments linked to sustAIn. 
- It contains also a model of a mixed-signal AI accelerator as well as a complete interfacing to it in C. 
- Link: https://gitlab.com/a-core/

### The SyDeKick modelling 
- The SyDeKick is a python-based framework enabling to model various parts of a system at multiple levels of abstraction.
- Any additional part can be modelled in Python and added to the system. 
- Link: https://github.com/TheSystemDevelopmentKit/



# Tools developped partly/totally within SustAIn

## Work Package 2

### Approximate Computing for probabilistic Circuits using Addition-As-Int
As a first contribution to sustain [S1] we have demonstrated the first approximate computing framework for PCs, 
which can significantly reduce the hardware footprint of PCs. 
- Link: https://github.com/lingyunyao/AAI_Probabilistic_Circuits


Reference [S1]: L. Yao, M. Trapp, J. Leslin, G. Singh, K. Periasamy, M. Andraud, “On hardware-efficient Inference 
in Probabilistic Circuits” arXiv (https://arxiv.org/abs/2405.13639)

### AutoPC

AutoPC is a tool to automatically generate Probabilistic Circuits [S2]. 
It can serve as a baseline implementation for anyone looking to use PCs.  
- Link: https://github.com/Karthekeyan-aalto/newcas_2024

Reference [S2]: K.Periasamy, J.Leslin, L.Yao, A. Korsman, M. Andraud, “AutoPC: an open-source framework for 
efficient probabilistic reasoning on FPGA hardware”, accepted in NEWCAS 2024, to appear. 

























